import java.util.Arrays;
import java.util.Comparator;

public class Test {
    public static void printStudents(Student[] students) {
        for (Student s : students) {
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        Student[] students = new Student[] {
                new Student(2001, "Ivan"),
                new Student(2000, "Peter"),
                new Student(2003, "Eugene"),
        };

        printStudents(students);

        System.out.println("---");

        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                if (o1.getYear() > o2.getYear()) {
                    return 1;
                }
                if (o1.getYear() < o2.getYear()) {
                    return -1;
                }
                return 0;
            }
        });

        printStudents(students);
    }
}
