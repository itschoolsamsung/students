public class Student {
    private Integer year;
    private String name;

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public Student(Integer year, String name) {
        setYear(year);
        setName(name);
    }

    @Override
    public String toString() {
        return year + " " + name;
    }
}
